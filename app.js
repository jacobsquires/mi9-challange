var 	express 		= require('express');
var 	app 			= express();
var 	bodyParser 		= require('body-parser');
const 	DEFAULT_PORT 	= 8080;


// sets port to default unless otherwise specified in the environment
app.set('port', process.env.PORT || DEFAULT_PORT);
//configure middleware and routes
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

/**
 * HTTP POST /
 * Body Param: the JSON task you want to filter
 * Returns: 200 HTTP code
 * Error: 400 HTTP code if the task doesn't exists
 */
app.post('/', function(req, res, next){
	//check body and payload exist and payload is an array
	if (!req.body){
		next(new Error("body"));
	}
	if (!req.body.payload){
		next(new Error("payload"));
	}
	if (req.body.payload.constructor !== Array) {
		next(new Error("payload not array"));
	}
	//filter array
	var filteredData = req.body.payload.filter(function(show){
		//check for required fields to compare and return later
		if (!show.image) return false;
		if (!show.image.showImage) return false;
		if (!show.drm) return false;
		if (!show.episodeCount) return false;
		if (!show.slug) return false;
		if (!show.title) return false;

		//check if drm && atlease one episode
		if(show.drm && show.episodeCount > 0){
			return true; 
		}
	});
	//map the fields we want
	var mappedData = filteredData.map(function(show){
		return {
			image: show.image.showImage,
			slug: show.slug,
			title: show.title
		};
	});
	//send JSON response
	res.status(200);
	res.send({
		response: mappedData
	});
});
/**
 * HTTP '*'
 * Returns: Error(Could not locate API endpoint)
 * Error: 500 HTTP code if the task doesn't exists
 */
app.use('*', function(req, res){
	res.status(500);
    res.json({
    	error: 'Could not locate API endpoint'
    });
});
//detect enviroment and ajust middleware
if ( app.get('env') === 'development' ) {
	// development error handler, will print stacktrace
    app.use(function(err, req, res, next) {
    	console.log(err);
		if (res.headersSent) {
			return next(err);
		}
	    res.status(400);
	    res.json({
	    	error: 'Could not decode request: JSON parsing failed'
	    });
	});
}
if ( app.get('env') === 'production' ) {
    //production error handler
    app.use(function(err, req, res, next) {
		if (res.headersSent) {
			return next(err);
		}
	    res.status(400);
	    res.json({
	    	error: 'Could not decode request: JSON parsing failed'
	    });
	});
}

app.listen(app.get('port'));
console.log('Server Started on PORT:' + app.get('port'));